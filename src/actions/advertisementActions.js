import axios from 'axios';

export const getAdvertisements = limit => axios.get(`/advertisement/limit=${limit}`);

import axios from 'axios';

export const getNews = (type, limit) => axios.get(`/news/getNews/${type}?limit=${limit}&skip=0`);

export const getSingleNews = id => axios.get(`/news/getIndividualNews/${id}`);

export const getHighlights = limit => axios.get(`/news/getNewsHighlights`);

export const getLatestNews = limit => axios.get(`/news/getLatestNews`);

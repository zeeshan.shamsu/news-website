import axios from 'axios';

export const addNews = requestObject => axios.post(`/news/addNews`, requestObject, {
  headers: {
    'Content-Type': 'multipart/form-data',
    'x-access-token': localStorage.token,
  }
});

export const deleteNewsAdmin = requestObject => axios.post(`/news/removeNews`, requestObject)

export const getNewsForAdmin = (type, limit) => axios.get(`/news/getNews/${type}?limit=${limit}`);

export const adminLogin = requestObject => axios.post('/adminLogin', requestObject);

import React from "react";
import ReactDOM from "react-dom";
import { render } from 'react-dom';

import App from "./components/app";

import { setAxiosConfig } from './utils/axiosHelpers';
import 'react-notifications/lib/notifications.css';
// import 'react-notifications-component/dist/theme.css'
import ReactNotification from 'react-notifications-component'
// import 'bootstrap/dist/css/bootstrap.min.css';


function renderApp() {
  render(
    <div>
      <ReactNotification />
      <App />
    </div>,
    document.getElementById('index'),
  );
}
setAxiosConfig();
renderApp();

// const Index = () => <App />;


// ReactDOM.render(<Index />, document.getElementById("index"));

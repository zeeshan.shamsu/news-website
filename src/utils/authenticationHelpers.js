import jwt from 'jsonwebtoken';
import config from '../config'


export const validateToken = () => {
  if (localStorage.token) {
    return new Promise((resolve, reject) => {
      jwt.verify(localStorage.token, config.jwtSecret, (err, decoded) => {
        if (err) {
          console.log('err: ', err);
          resolve(false);
        } else {
          resolve(true);
        }
      });
    });

  }
  return false;
}
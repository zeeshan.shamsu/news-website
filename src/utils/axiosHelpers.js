import axios from 'axios';
import config from '../config';

export const setAxiosConfig = () => {
  axios.defaults.baseURL = config.apiAddress;
}
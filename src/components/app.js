import React, { Component, Fragment } from "react";
import ReactDOM from "react-dom";
import { Switch, Route } from 'react-router';
import { Router } from 'react-router-dom';
import Home from './home';
import SingleNews from "./singleNews";
import Categories from "./categories";
import AddNews from "./admin/addNews";
import NewsList from "./admin/newsList";
import Login from "./admin/login";
import { NotificationContainer } from 'react-notifications';
import browserHistory from './browserHistory';
import AboutUs from "./aboutUs";
import Contact from "./contact";

const wrapper = {
  Home: Home,
  SingleNews: SingleNews,
  Categories: Categories,
  AddNews: AddNews,
  NewsList: NewsList,
  Login,
}
class App extends Component {

  render() {
    return (
      <Router history={browserHistory}>
        <Fragment>
          <Switch>
            <Route exact path="/" component={(props) => <Home {...props} />} />
            <Route exact path="/about_us" component={(props) => <AboutUs {...props} />} />
            <Route exact path="/contact" component={(props) => <Contact {...props} />} />
            <Route exact path="/single/:id" component={wrapper.SingleNews} />
            <Route exact path="/categories/:type" component={wrapper.Categories} />

            <Route exact path="/admin/login" component={wrapper.Login} />
            <Route exact path="/admin/" component={wrapper.NewsList} />
            <Route exact path="/admin/news/add" component={wrapper.AddNews} />

          </Switch>
        </Fragment>
      </Router>
    );
  };
}

export default App;


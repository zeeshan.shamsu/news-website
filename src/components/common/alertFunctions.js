import { NotificationManager } from 'react-notifications';
import Toast from 'light-toast';
import { store } from 'react-notifications-component';

export const alertInfo = (title = '', message = '', timeout = 5000) => {
  // NotificationManager.info(message, title, timeout);
  Toast.info(title, timeout);
};

export const alertSuccess = (title = '', message = '', timeout = 5000) => {
  // Toast.success(title, timeout);
  store.addNotification({
    title: title,
    message: message || ' ',
    type: "success",
    insert: "top",
    container: "top-right",
    animationIn: ["animated", "fadeIn"],
    animationOut: ["animated", "fadeOut"],
    dismiss: {
      duration: timeout,
      onScreen: true
    }
  });
  // NotificationManager.success(message, title, timeout);
};

export const alertError = (title = '', message = '', timeout = 5000) => {
  // Toast.fail(title, timeout);
  store.addNotification({
    title: title,
    message: message || ' ',
    type: "warning",
    insert: "top",
    container: "top-right",
    animationIn: ["animated", "fadeIn"],
    animationOut: ["animated", "fadeOut"],
    dismiss: {
      duration: timeout,
      onScreen: true
    }
  });
  // NotificationManager.error(message, title, timeout);
};


import React, { Component } from 'react';
import SingleCategory from './singleCategory';
import Header from '../header';
import { validateToken } from '../../utils/authenticationHelpers';



class NewsList extends Component {

  componentDidMount = async () => {
    const validateTokenResult = await validateToken();
    if (!validateTokenResult) {
      this.props.history.push('/admin/login');
    }
  }

  onClickAddNews = () => {
    this.props.history.push('/admin/news/add')
  }

  render() {
    return (
      <div>

        <div style={{ position: 'fixed' }}>
          {/* <div id="status">&nbsp;</div> */}
        </div>
        <a className="scrollToTop" href="#"><i className="fa fa-angle-up"></i></a>

        <div className="container">

          <Header {...this.props} />

          <section id="contentSection">
            <a onClick={this.onClickAddNews} style={{ cursor: 'pointer', textDecoration: 'underline', color: 'blue' }}>Click here to add news</a>
            <br />
            <br />
            <div className="row"></div>
            <SingleCategory type="kerala" {...this.props} />
            <SingleCategory type="national" {...this.props} />
            <SingleCategory type="special" {...this.props} />
            <SingleCategory type="entertainment" {...this.props} />
            <SingleCategory type="sports" {...this.props} />
            <SingleCategory type="politics" {...this.props} />
            <SingleCategory type="technology" {...this.props} />
            <SingleCategory type="business" {...this.props} />
            <SingleCategory type="health" {...this.props} />
            {/* <SingleCategory type="pravasi" {...this.props} /> */}
            <SingleCategory type="crime" {...this.props} />
            <SingleCategory type="editorial" {...this.props} />
            <SingleCategory type="food" {...this.props} />
          </section>
        </div>
      </div>
    )
  }
}

export default NewsList;


import React, { Component } from 'react';
import { getNewsForAdmin, deleteNewsAdmin } from '../../actions/adminActions';
import { FaMinus } from 'react-icons/fa';
import { alertSuccess, alertError } from '../common/alertFunctions';


const typeMapper = {
  kerala: 'kerala',
  national: 'national',
  special: 'special',
  entertainment: 'entertainment',
  sports: 'sports',
  politics: 'politics',
  technology: 'technology',
  business: 'business/travel',
  health: 'health',
  pravasi: 'pravasi',
  crime: 'crime',
  editorial: 'editorial',
  food: 'food',
}

class SingleCategory extends Component {

  constructor() {
    super();
    this.state = {
      news: [],
    }
  }

  componentDidMount = async () => {
    this.fetchNewsList();
  }

  onClickDeleteNews = async (id) => {
    try {
      await deleteNewsAdmin({ newsid: id });
      alertSuccess('News deleted');
      this.fetchNewsList()
    } catch (error) {
      console.log('error: ', error);
      alertError('Internal Server Error');
    }
  }

  fetchNewsList = async () => {
    try {
      const { type } = this.props;
      /* uncomment this once the API is working */
      const newsResponse = await getNewsForAdmin(type, 20);
      // console.log('newsResponse: ', newsResponse);
      const { data: news } = newsResponse.data;
      this.setState({ news });
    } catch (error) {
      console.log('error: ', error);
    }
  }

  onClickNews = (id) => {
    this.props.history.push(`/single/${id}`)
  }

  displayNewsList = () => {
    return this.state.news.map((singleNews, index) => {
      return (
        <li key={index}>
          <div className="media wow fadeInDown ">
            <a onClick={() => this.onClickNews(singleNews.id)} className="media-left">
              <img alt="" src={singleNews.image_url} />
            </a>
            <div className="media-body pd-r-5">
              <a onClick={() => this.onClickDeleteNews(singleNews.id)}><FaMinus color='red' size='1.5em' /></a>
            </div>
            <div className="media-body">
              <a onClick={() => this.onClickNews(singleNews.id)} className="catg_title" style={{ cursor: 'pointer' }}>
                {singleNews.heading}
              </a>
            </div>
          </div>
        </li>
      )
    })

  }

  render() {
    const { type } = this.props;
    return (
      <div className="fashion">
        <div className="single_post_content">
          <h2><span>{typeMapper[type]}</span></h2>
          <ul className="spost_nav">

            {this.displayNewsList()}

          </ul>
        </div>
      </div>
    )
  }
}

export default SingleCategory;


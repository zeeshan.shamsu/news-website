import React, { Component } from 'react';
import { addNews } from '../../actions/adminActions';
import Advertisements from '../advertisements';
import Footer from '../footer';
import Header from '../header';
import { alertInfo, alertError, alertSuccess } from '../common/alertFunctions';
import Spinner from 'react-bootstrap/Spinner'
import BeatLoader from "react-spinners/BeatLoader";
import { validateToken } from '../../utils/authenticationHelpers';

const override = `
  display: block;
  margin: 0 auto;
  border-color: red;
`;

class AddNews extends Component {
  constructor(props) {
    super(props);
    this.state = {
      heading: '',
      description: '',
      short_description: '',
      type: 'politics',
      isHighlight: false,
      isLatest: false,
      isLoading: false,
    };
  }

  componentDidMount = async () => {
    const validateTokenResult = await validateToken();
    if (!validateTokenResult) {
      this.props.history.push('/admin/login');
    }
  }
  
  onChangeHandler = (event) => {
    let nam = event.target.name;
    let val = event.target.value;
    this.setState({ [nam]: val });
  }

  onChangeFileHandler = (event) => {
    this.setState({
      selectedFile: event.target.files[0],
    })
  }

  onClickSubmit = async () => {
    this.setState({ isLoading: true });
    try {
      const requiredFields = ['heading', 'description', 'short_description'];
      const emptyField = requiredFields.find(field => !this.state[field]);
      if (emptyField) {
        alertError(`please enter ${emptyField}`);
        this.setState({ isLoading: false });
        return;
      }
      const updateObject = new FormData()
      updateObject.append('file', this.state.selectedFile)
      const data = [
        { name: "heading", value: this.state.heading },
        { name: "description", value: this.state.description },
        { name: "short_description", value: this.state.short_description },
        { name: "type", value: this.state.type },
        { name: "isHighlight", value: this.state.isHighlight === 'on' ? true : false },
        { name: "isLatest", value: this.state.isLatest === 'on' ? true : false },
      ]
      updateObject.append('data', JSON.stringify(data))
      await addNews(updateObject);
      alertSuccess('News Added');
      // this.props.history.push('/');
    } catch (error) {
      console.log('error: ', error);
      alertError('Internal Server Error');
    }
    this.setState({ isLoading: false });
  }

  render() {
    const { isLoading } = this.state;
    return (

      <div>
        <div style={{ position: 'fixed' }}>
          {/* <div id="status">&nbsp;</div> */}
        </div>
        <a className="scrollToTop" href="#"><i className="fa fa-angle-up"></i></a>
        <div className="container">

          <Header {...this.props} />


          <section id="contentSection">
            <div className="row">
              <div className="col-lg-8 col-md-8 col-sm-8">
                <div className="left_content">
                  <div className="fashion_technology_area">



                    <form>
                      heading:
                      <br />

                      <input
                        type='text'
                        name='heading'
                        onChange={this.onChangeHandler}
                      />


                      <br />
                      <br />
                      short_description:
                      <br />
                      <textarea
                        style={{ height: '100px', width: '200px' }}
                        name='short_description'
                        value={this.state.short_description}
                        onChange={this.onChangeHandler}
                      />

                      <br />
                      <br />
                      description:
                      <br />
                      <textarea
                        style={{ height: '200px', width: '200px' }}
                        name='description'
                        value={this.state.description}
                        onChange={this.onChangeHandler}
                      />



                      <br />
                      <br />
                      type:&nbsp;&nbsp;
                      <select name='type' onChange={this.onChangeHandler} defaultValue="politics">
                        <option value="kerala">kerala</option>
                        <option value="national">national</option>
                        <option value="special">special</option>
                        <option value="entertainment">entertainment</option>
                        <option value="sports">sports</option>
                        <option value="politics">politics</option>
                        <option value="technology">technology</option>
                        <option value="business">business/travel</option>
                        <option value="health">health</option>
                        <option value="pravasi">pravasi</option>
                        <option value="crime">crime</option>
                        <option value="editorial">editorial</option>
                        <option value="food">food</option>


                      </select>

                      <br />
                      <br />

                      Highlighted News:&nbsp; &nbsp;
                      <input
                        type="checkbox"
                        name="isHighlight"
                        defaultChecked={this.state.isHighlight}
                        onChange={this.onChangeHandler}
                      />

                      <br />
                      <br />


                      Latest News:&nbsp; &nbsp;
                      <input
                        type="checkbox"
                        name="isLatest"
                        defaultChecked={this.state.isLatest}
                        onChange={this.onChangeHandler}
                      />

                      <br />
                      <br />

                      <input type="file" name="file" onChange={this.onChangeFileHandler} />

                      <br />
                      <br />

                      <BeatLoader
                        css={override}
                        size={10}
                        color={"green"}
                        loading={isLoading}
                      />
                      {!isLoading && <button type="button" className="btn btn-success" onClick={this.onClickSubmit}>Upload</button>}
                      <br />
                      <br />
                      <br />
                    </form>


                  </div>
                </div>
              </div>
              <Advertisements />
            </div>
          </section>
        </div>

        <Footer />
      </div >



    );
  }
}

export default AddNews;

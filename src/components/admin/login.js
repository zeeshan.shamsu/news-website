import React, { Component } from 'react';
import { alertError, alertSuccess } from '../common/alertFunctions';
import { adminLogin } from '../../actions/adminActions';

class Login extends Component {

  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: '',
      isLoading: false,
    };
  }

  onChangeHandler = (event) => {
    let nam = event.target.name;
    let val = event.target.value;
    this.setState({ [nam]: val });
  }

  onClickSubmit = async () => {
    this.setState({ isLoading: true });
    try {
      const { username, password } = this.state;
      const requiredFields = ['username', 'password',];
      const emptyField = requiredFields.find(field => !this.state[field]);
      if (emptyField) {
        alertError(`please enter ${emptyField}`);
        this.setState({ isLoading: false });
        return;
      }
      const updateObject = { username, password };
      const loginResponse = await adminLogin(updateObject);
      if (loginResponse.data) {
        const { jwtToken } = loginResponse.data;
        localStorage.setItem('token', jwtToken)
        alertSuccess('Successfully logged in');
        this.props.history.push('/admin/')
      }
    } catch (error) {
      console.log('error: ', error);
      alertError('Username and password does not match');
    }
    this.setState({ isLoading: false });
  }

  render() {
    return (
      <div className="Flex--flex Flex-direction--row Flex-wrap-wrap Flex-justifyContent--center">
        <div>
          <br />
          <br />
          <h3>Admin Login</h3>
          <br />
          <input
            style={{ width: '200px' }}
            type='text'
            name='username'
            onChange={this.onChangeHandler}
          />
          <br />
          <br />
          <input
            style={{ width: '200px' }}
            type='password'
            name='password'
            onChange={this.onChangeHandler}
          />
          <br />
          <br />
          <button type="button" onClick={this.onClickSubmit}>Login</button>


        </div>
      </div>
    )
  }
}

export default Login;

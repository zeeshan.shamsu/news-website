import React, { Component } from 'react';
import Header from './header';
import Footer from './footer';
import Advertisements from './advertisements';
import { getNews } from '../actions/newsActions';

class Categories extends Component {


  constructor() {
    super();
    this.state = {
      news: [
        // {
        //   /* Temp data */
        //   id: 1,
        //   image_url: '/public/images/news2.jpg',
        //   heading: 'ജാമിയ ഹർജി ഫെബ്രുവരി നാലിലേക്ക് മാറ്റി 2',
        //   short_description: 'സിഎ പുനര്‍മൂല്യനിര്‍ണയം: വിദ്യാര്‍ഥികള്‍ക്ക് പിന്തുണയുമായി രാഹുല്‍ ഗാന്ധി......',
        // },
        // {
        //   /* Temp data */
        //   id: 2,
        //   image_url: '/public/images/news2.jpg',
        //   heading: 'ജാമിയ ഹർജി ഫെബ്രുവരി നാലിലേക്ക് മാറ്റി 2',
        //   short_description: 'സിഎ പുനര്‍മൂല്യനിര്‍ണയം: വിദ്യാര്‍ഥികള്‍ക്ക് പിന്തുണയുമായി രാഹുല്‍ ഗാന്ധി......',
        // }
      ],
    }
  }

  componentDidMount = async () => {
    this.fetchNewsList();
  }

  componentDidUpdate = (prevProps) => {
    if (this.props.match.params.type !== prevProps.match.params.type) {
      this.fetchNewsList();
    }
  }

  fetchNewsList = async () => {
    try {
      const { type } = this.props.match.params;
      // console.log('type: ', type);
      /* uncomment this once the API is working */
      const newsResponse = await getNews(type, 20);
      const { data: news } = newsResponse.data;
      // const news = data.slice(0, 6);
      // console.log('news: ', news);
      this.setState({ news });
    } catch (error) {
      console.log('error: ', error);
    }
  }

  onClickNews = (id) => {
    this.props.history.push(`/single/${id}`)
  }

  displayNewsList = () => {
    return this.state.news.map((singleNews, index) => {
      return (
        <li key={index}>
          <div className="Flex--flex Flex-direction--row Flex-wrap-wrap pd-b-20">
            <img className="categories-image" alt="" src={singleNews.image_url} />
            <div className="pd-l-20 categories-div">
              <a onClick={() => this.onClickNews(singleNews.id)}>
                <b>{singleNews.heading}</b>
              </a>
              <p>
                {singleNews.short_description}
              </p>
            </div>
          </div>
        </li>
      )
    })
  }


  render() {
    return (

      <div>
        {/* <div id="preloader">
          <div id="status">&nbsp;</div>
        </div> */}
        <a className="scrollToTop" href="#"><i className="fa fa-angle-up"></i></a>
        <div className="container">

          <Header {...this.props} />


          <section id="contentSection">
            <div className="row">
              <div className="col-lg-8 col-md-8 col-sm-8">
                <div className="left_content">
                  <div className="single_post_content">

                    <ul className="business_catgnav  wow fadeInDown">
                      {this.displayNewsList()}
                    </ul>
                  </div>
                </div>
              </div>

              <Advertisements />

            </div>
          </section>

        </div>


        <Footer />
      </div >
    )
  }
};

export default Categories;
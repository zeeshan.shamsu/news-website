import React, { Component } from 'react';
import { getAdvertisements } from '../actions/advertisementActions';

class Advertisements extends Component {

  constructor() {
    super();
    this.state = {
      advertisements: [{
        /* Temp data */
        id: 1,
        image_url: '/public/images/ad1.gif',
      }],
    }
  }

  componentDidMount = async () => {
    this.fetchAdvertisementList();
  }


  fetchAdvertisementList = async () => {
    try {
      const { type } = this.props;
      /* uncomment this once the API is working */
      // const response = await getAdvertisements(5);
      // const { advertisements } = response.data;
      // this.setState({ advertisements });
    } catch (error) {
      console.log('error: ', error);
    }
  }

  displayList = () => {
    return this.state.advertisements.map((ad, index) => <a key={index} className="sideAdd" href="#"><img src={ad.image_url} alt="" /></a>)
  }

  render() {
    return (
      <div className="col-lg-4 col-md-4 col-sm-4">
        <aside className="right_content">
          {this.displayList()}
        </aside>
      </div>
    )
  }
}

export default Advertisements;


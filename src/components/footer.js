import React, { Component } from 'react';

class Footer extends Component {
  render() {
    return (
      <footer id="footer">

        <div className="footer_bottom">
          <p className="developer">Vartha Malayalam</p>
        </div>
      </footer>
    )
  }
}

export default Footer;
import React, { Component } from 'react';
import Header from "./header";
import Footer from "./footer";
import Advertisements from "./advertisements";
import Highlights from "./home/highlights";
import LatestNews from "./home/latestNews";


class AboutUs extends Component {


  render() {
    return (
      <div>

        <div style={{ position: 'fixed' }}>
          {/* <div id="status">&nbsp;</div> */}
        </div>
        <a className="scrollToTop" href="#"><i className="fa fa-angle-up"></i></a>
        <div className="container">

          <Header {...this.props} />



          <section id="contentSection">
            <div className="row">
              <div className="col-lg-8 col-md-8 col-sm-8">
                <div className="left_content">
                  <div className="single_page">
                    <h1>About US</h1>
                    <div className="single_page_content">
                    വിശ്വാസ്യത മുഖമുദ്രയാക്കിയ മലയാളിയുടെ വാർത്താ പോർട്ടൽ. അതാണ് വാർത്താ മലയാളം . ഇവിടെ വളച്ചൊടിച്ച വാർത്തകൾക്ക് സ്ഥാനമില്ല . വ്യാജ വാർത്തകളും പടിക്ക് പുറത്ത്. കഴിവും അനുഭവസമ്പത്തും കൈകോർക്കുമ്പോൾ മലയാളിക്ക് അനുപമമായ വാർത്താ അനുഭവം വാർത്താ മലയാളം ഉറപ്പു നൽകുന്നു. ഏറ്റവും പുതിയ വാർത്തകളും വിശകലനങ്ങളും ചൂടാറാതെ , നിങ്ങളുടെ വിരൽത്തുമ്പിൽ. സ്വീകരിച്ചാലും....
                    </div>
                  </div>
                </div>

              </div>
              <Advertisements />
            </div>
          </section>
        </div>

        <Footer />
      </div >
    )
  }
}

export default AboutUs;
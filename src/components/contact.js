import React, { Component } from 'react';
import Header from "./header";
import Footer from "./footer";
import Advertisements from "./advertisements";
import Highlights from "./home/highlights";
import LatestNews from "./home/latestNews";


class Contact extends Component {


  render() {
    return (
      <div>

        <div style={{ position: 'fixed' }}>
          {/* <div id="status">&nbsp;</div> */}
        </div>
        <a className="scrollToTop" href="#"><i className="fa fa-angle-up"></i></a>
        <div className="container">

          <Header {...this.props} />



          <section id="contentSection">
            <div className="row">
              <div className="col-lg-8 col-md-8 col-sm-8">
                <div className="left_content">
                  <div className="single_page">
                    <h1>Contact</h1>
                    <div className="single_page_content">
                    Second Floor, Jubilee Bazar Complex , Kannur.
                    </div>
                  </div>
                </div>

              </div>
              <Advertisements />
            </div>
          </section>
        </div>

        <Footer />
      </div >
    )
  }
}

export default Contact;
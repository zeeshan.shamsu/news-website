import React, { Component } from 'react';
import moment from 'moment';
// import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import 'font-awesome/css/font-awesome.min.css';
import { FaFacebook } from 'react-icons/fa';
import config from '../config';

const monthMapper = {
  '01': 'Jan',
  '1': 'Jan',
  '02': 'Feb',
  '2': 'Feb',
  '03': 'Mar',
  '3': 'Mar',
  '04': 'Apr',
  '4': 'Apr',
  '05': 'May',
  '5': 'May',
  '06': 'Jun',
  '6': 'Jun',
  '07': 'Jul',
  '7': 'Jul',
  '08': 'Aug',
  '8': 'Aug',
  '09': 'Sep',
  '9': 'Sep',
  '10': 'Oct',
  '11': 'Nov',
  '12': 'Dec',
}

class Header extends Component {

  onClickCategory = (category) => {
    this.props.history.push(`/categories/${category}`)
  }

  onClickHome = () => {
    this.props.history.push('/');
  }

  onClickAboutUs = () => {
    this.props.history.push('/about_us');
  }

  onClickContact = () => {
    this.props.history.push('/contact');
  }

  render() {

    const currentMonth = monthMapper[moment().format('MM')];
    const currentYear = moment().format('YYYY');
    const currentTime = moment().format('DD');
    const currentDay = moment().format('dddd');


    return (
      <div>
        <header id="header">
          <div className="row">
            <div className="col-lg-12 col-md-12 col-sm-12">

              <div className="header_top">
                <div className="header_top_left">
                  <ul className="top_nav">
                    <li><a onClick={this.onClickHome}>Home</a></li>
                    <li><a onClick={this.onClickAboutUs}>About</a></li>
                    <li><a onClick={this.onClickContact}>Contact</a></li>
                  </ul>
                </div>
                <div className="header_top_right">
                  {/* <i className="fa fa-facebook-official" aria-hidden="true">sss</i> */}
                  {/* <FontAwesomeIcon icon="coffee" /> */}
                  <a target="_blank" href={config.facebookURL}><FaFacebook color='white' size='2.9em' className="pd-t-15" /></a>
                </div>
              </div>

              {/* <div className="header_top">
                <div className="header_top_right">
                  <p>Friday, December 05, 2045</p>
                </div>
              </div> */}


            </div>

            {/* <div className="display-inline-block pd-l-20 pd-t-10 pd-b-10 black">
              {(new Date("MM/DD/YYYY")).toString()}
            </div> */}

            <div className="col-lg-12 col-md-12 col-sm-12">
              <div className="header_bottom">

                <div className="logo_area black" style={{ width: '10%' }}>
                  <div>
                    <div className="black font-size-15 font-weight-500">{currentMonth}/{currentYear}</div>
                    <div className="blue font-size-33" style={{ lineHeight: '35px' }}><b>{currentTime}</b></div>
                    <div className="black font-size-15 font-weight-500">{currentDay}</div>
                  </div>

                </div>
                <div className="logo_area">
                  <a onClick={this.onClickHome} className="logo">
                    <img src="/public/images/logo.jpeg" alt="" />
                  </a>
                </div>
                <div className="text-align-right">
                  <img className="sponsor-img" src="/public/images/sponsor.jpg" height='90px' alt="" />
                </div>

              </div>
            </div>
          </div>
        </header>


        <section id="navArea">
          <nav className="navbar navbar-inverse" role="navigation">
            <div className="navbar-header">
              <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                aria-expanded="false" aria-controls="navbar"> <span className="sr-only">Toggle navigation</span> <span
                  className="icon-bar"></span> <span className="icon-bar"></span> <span className="icon-bar"></span> </button>
            </div>
            <div id="navbar" className="navbar-collapse collapse">
              <ul className="nav navbar-nav main_nav">
                <li className="active"><a onClick={this.onClickHome}><span className="fa fa-home desktop-home"></span><span
                  className="mobile-show">Home</span></a></li>
                <li><a onClick={() => this.onClickCategory('kerala')}>kerala</a></li>
                <li><a onClick={() => this.onClickCategory('national')}>national</a></li>
                <li><a onClick={() => this.onClickCategory('special')}>special</a></li>
                <li><a onClick={() => this.onClickCategory('entertainment')}>entertainment</a></li>
                <li><a onClick={() => this.onClickCategory('sports')}>sports</a></li>
                <li><a onClick={() => this.onClickCategory('politics')}>politics</a></li>
                <li><a onClick={() => this.onClickCategory('technology')}>technology</a></li>
                <li><a onClick={() => this.onClickCategory('business')}>business/travel</a></li>
                <li><a onClick={() => this.onClickCategory('health')}>health</a></li>
                <li><a onClick={() => this.onClickCategory('pravasi')}>pravasi</a></li>
                <li><a onClick={() => this.onClickCategory('crime')}>crime</a></li>
                <li><a onClick={() => this.onClickCategory('editorial')}>editorial</a></li>
                <li><a onClick={() => this.onClickCategory('food')}>food</a></li>


              </ul>
            </div>
          </nav>
        </section>
      </div>
    )
  }
}

export default Header;
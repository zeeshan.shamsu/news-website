import React, { Component } from 'react';
import { getNews } from '../../actions/newsActions';


const typeMapper = {
  kerala: 'kerala',
  national: 'national',
  special: 'special',
  entertainment: 'entertainment',
  sports: 'sports',
  politics: 'politics',
  technology: 'technology',
  business: 'business/travel',
  health: 'health',
  pravasi: 'pravasi',
  crime: 'crime',
  editorial: 'editorial',
  food: 'food',
}

class HomeCategories extends Component {

  constructor() {
    super();
    this.state = {
      news: [
        // {
        //   /* Temp data */
        //   id: 1,
        //   image_url: '/public/images/news2.jpg',
        //   heading: 'ജാമിയ ഹർജി ഫെബ്രുവരി നാലിലേക്ക് മാറ്റി 2',
        // },
        // {
        //   /* Temp data */
        //   id: 2,
        //   image_url: '/public/images/news2.jpg',
        //   heading: 'ജാമിയ ഹർജി ഫെബ്രുവരി നാലിലേക്ക് മാറ്റി 2',
        // }
      ],
    }
  }

  componentDidMount = async () => {
    this.fetchNewsList();
  }

  fetchNewsList = async () => {
    try {
      const { type } = this.props;
      /* uncomment this once the API is working */
      const newsResponse = await getNews(type, 4);
      // console.log('newsResponse: ', newsResponse);
      const { data: news } = newsResponse.data;
      // const news = data.slice(0, 4);
      this.setState({ news });
    } catch (error) {
      console.log('error: ', error);
    }
  }

  onClickNews = (id) => {
    this.props.history.push(`/single/${id}`)
  }

  displayNewsList = () => {
    return this.state.news.map((singleNews, index) => {
      return (
        <li key={index}>
          <div className="media wow fadeInDown highlight-div">
            <a onClick={() => this.onClickNews(singleNews.id)} className="media-left">
              <img alt="" src={singleNews.image_url} />
            </a>
            <div className="media-body">
              <a onClick={() => this.onClickNews(singleNews.id)} className="catg_title" style={{ cursor: 'pointer' }}>
                {singleNews.heading}
              </a>
            </div>
          </div>
        </li>
      )
    })

  }

  render() {
    const { type } = this.props;
    return (
      <div className="fashion" style={{ height: '428px' }}>
        <div className="single_post_content">
          <h2><span>{typeMapper[type]}</span></h2>
          <ul className="spost_nav">

            {this.displayNewsList()}

          </ul>
        </div>
      </div>
    )
  }
}

export default HomeCategories;


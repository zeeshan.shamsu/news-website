import React, { Component } from 'react';
import { getHighlights } from '../../actions/newsActions';
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import Slider from "react-slick";


class Highlights extends Component {


  constructor() {
    super();
    this.state = {
      news: [
        // {
        //   /* Temp data */
        //   id: 1,
        //   image_url: '/public/images/news2.jpg',
        //   heading: 'ജാമിയ ഹർജി ഫെബ്രുവരി നാലിലേക്ക് മാറ്റി 2',
        //   short_description: 'സിഎ പുനര്‍മൂല്യനിര്‍ണയം: വിദ്യാര്‍ഥികള്‍ക്ക് പിന്തുണയുമായി രാഹുല്‍ ഗാന്ധി......',
        // },
        // {
        //   /* Temp data */
        //   id: 2,
        //   image_url: '/public/images/news2.jpg',
        //   heading: 'ജാമിയ ഹർജി ഫെബ്രുവരി നാലിലേക്ക് മാറ്റി 2',
        // }
      ],
    }
  }

  componentDidMount = async () => {
    // console.log('Highlights componentDidMount: ');
    this.fetchNewsList();
  }

  componentWillUnmount = () => {
    // console.log('highlights -----------------');
  }

  fetchNewsList = async () => {
    try {
      const { type } = this.props;
      /* uncomment this once the API is working */
      const newsResponse = await getHighlights(5);
      const { data: news } = newsResponse.data;
      // const news = data.slice(0, 6);
      // console.log('news: ', news);
      this.setState({ news });
    } catch (error) {
      console.log('error: ', error);
    }
  }

  onClickNews = (id) => {
    this.props.history.push(`/single/${id}`)
  }


  displayNewsList = () => {
    // console.log('this.state.news: ', this.state.news);
    return this.state.news.map((singleNews, index) => {
      return (
        <div key={index} className="single_iteam">
          <a onClick={() => this.onClickNews(singleNews.id)} style={{ cursor: 'pointer' }}>
            <img src={singleNews.image_url} alt="" />
          </a>
          <div className="slider_article">
            <h2>
              <a className="slider_tittle" onClick={() => this.onClickNews(singleNews.id)} style={{ cursor: 'pointer' }}>
                {singleNews.heading}
              </a>
            </h2>
            {/* <p>{singleNews.short_description}</p> */}
          </div>
        </div>
      )
    })

  }

  render() {
    var settings = {
      dots: true,
      infinite: true,
      speed: 800,
      slidesToShow: 1,
      slide: 'div',
      autoplay: true,
      autoplaySpeed: 5000,
      easing: 'linear',
      fade: true,
      arrows: true,
      accessibility: true,
      draggable: true,
      swipe: true,
    };
    return (
      <div className="col-lg-8 col-md-8 col-sm-8">

        <div className="container">
          <Slider {...settings}>
            {this.state.news.map((singleNews, index) => {
              return (<div key={index}>
                <img onClick={() => this.onClickNews(singleNews.id)} className="slider-image" src={singleNews.image_url} />
                <div className="slider_article">
                  <h2><a className="slider_tittle" onClick={() => this.onClickNews(singleNews.id)}>{singleNews.heading}</a></h2>
                  {/* <p>{singleNews.short_description}</p> */}
                </div>
              </div>)
            })}
          </Slider>
        </div>
      </div >
    );
  }

  // render() {
  //   return (
  //     <div className="col-lg-8 col-md-8 col-sm-8">
  //       <div className="slick_slider">

  //         {this.displayNewsList()}

  //       </div>
  //     </div>
  //   )
  // }
}

export default Highlights;
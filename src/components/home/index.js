import React, { Component } from 'react';
import Header from "../header";
import Footer from "../footer";
import Advertisements from "../advertisements";
import Highlights from "./highlights";
import LatestNews from "./latestNews";
import HomeCategories from './homeCategories';


class Home extends Component {


  render() {
    return (
      <div>

        <div style={{ position: 'fixed' }}>
          {/* <div id="status">&nbsp;</div> */}
        </div>
        <a className="scrollToTop" href="#"><i className="fa fa-angle-up"></i></a>
        <div className="container">

          <Header {...this.props} />

          <section id="sliderSection">
            <div className="row" >
              <Highlights {...this.props} />
              <LatestNews {...this.props} />
            </div>
          </section>


          <section id="contentSection">
            <div className="row">
              <div className="col-lg-8 col-md-8 col-sm-8">
                <div className="left_content">
                  <div className="fashion_technology_area">

                    <HomeCategories type='kerala' {...this.props} />
                    <HomeCategories type='national' {...this.props} />
                    <HomeCategories type='special' {...this.props} />
                    <HomeCategories type='entertainment' {...this.props} />
                    <HomeCategories type='sports' {...this.props} />
                    <HomeCategories type='politics' {...this.props} />
                    <HomeCategories type='technology' {...this.props} />
                    <HomeCategories type='business' {...this.props} />
                    <HomeCategories type='health' {...this.props} />
                    <HomeCategories type='pravasi' {...this.props} />
                    <HomeCategories type='crime' {...this.props} />
                    <HomeCategories type='editorial' {...this.props} />
                    <HomeCategories type='food' {...this.props} />



                  </div>
                </div>
              </div>
              <Advertisements />
            </div>
          </section>
        </div>

        <Footer />
      </div >
    )
  }
}

export default Home;
import React, { Component } from 'react';
import { getLatestNews } from '../../actions/newsActions';

class LatestNews extends Component {

  constructor() {
    super();
    this.state = {
      news: [
        // {
        //   /* Temp data */
        //   id: 1,
        //   image_url: '/public/images/news2.jpg',
        //   heading: 'ജാമിയ ഹർജി ഫെബ്രുവരി നാലിലേക്ക് മാറ്റി 2',
        //   short_description: 'സിഎ പുനര്‍മൂല്യനിര്‍ണയം: വിദ്യാര്‍ഥികള്‍ക്ക് പിന്തുണയുമായി രാഹുല്‍ ഗാന്ധി......',
        // },
        // {
        //   /* Temp data */
        //   id: 2,
        //   image_url: '/public/images/news2.jpg',
        //   heading: 'ജാമിയ ഹർജി ഫെബ്രുവരി നാലിലേക്ക് മാറ്റി 2',
        // }
      ],
    }
  }

  componentDidMount = async () => {
    this.fetchNewsList();
  }

  fetchNewsList = async () => {
    try {
      const { type } = this.props;
      /* uncomment this once the API is working */
      const newsResponse = await getLatestNews(5);
      const { data: news } = newsResponse.data;
      // const news = data.slice(0, 6);
      // console.log('news: ', news);
      this.setState({ news });
    } catch (error) {
      console.log('error: ', error);
    }
  }

  onClickNews = (id) => {
    this.props.history.push(`/single/${id}`)
  }


  displayNewsList = () => {
    return this.state.news.map((singleNews, index) => {
      return (
        <li key={index}>
          <div className="media">
            <a onClick={() => this.onClickNews(singleNews.id)} className="media-left">
              <img alt="" src={singleNews.image_url} /> </a>
            <div className="media-body">
              <a onClick={() => this.onClickNews(singleNews.id)} className="catg_title"> {singleNews.heading}
              </a>
            </div>
          </div>
        </li>
      )
    })

  }

  render() {
    return (
      <div className="col-lg-4 col-md-4 col-sm-4">
        <div className="latest_post">
          <h2><span>Latest News</span></h2>
          <div className="latest_post_container">
            {/* <div id="prev-button"><i className="fa fa-chevron-up"></i></div> */}
            <ul className="latest_postnav">

              {this.displayNewsList()}

            </ul>
            {/* <div id="next-button"><i className="fa  fa-chevron-down"></i></div> */}
          </div>
        </div>
      </div>
    )
  }
}

export default LatestNews;
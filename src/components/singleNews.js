import React, { Component } from 'react';
import Advertisements from './advertisements';
import Header from './header';
import Footer from './footer';
import { getSingleNews } from '../actions/newsActions';

class SingleNews extends Component {

  constructor() {
    super();
    this.state = {
      news: {
        /* Temp data */
        // id: 1,
        // image_url: '/public/images/news2.jpg',
        // heading: 'ജാമിയ ഹർജി ഫെബ്രുവരി നാലിലേക്ക് മാറ്റി 2',
        // description: 'ജാമിയ സംഘര്‍ഷവുമായി ബന്ധപ്പെട്ട ഹര്‍ജികള്‍ ഫെബ്രുവരി നാലിന് പരിഗണിക്കാമെന്ന ഡല്‍ഹി ഹൈക്കോടതി തീരുമാനത്തിനെതിരെ അഭിഭാഷകര്‍. എത്രയും വേഗം ഹര്‍ജികള്‍ പരിഗണിക്കണമെന്നും വിദ്യാര്‍ഥികള്‍ക്ക് .',
      },
    }
  }

  componentDidMount = async () => {
    try {
      const { id } = this.props.match.params;
      /* To uncomment */
      const newsResponse = await getSingleNews(id);
      const { data: news } = newsResponse.data;
      this.setState({ news });
    } catch (error) {
      console.log('error: ', error);
    }
  }

  render() {
    const { news } = this.state;
    return (
      <div>
        {/* <div id="preloader">
          <div id="status">&nbsp;</div>
        </div> */}
        <a className="scrollToTop" href="#"><i className="fa fa-angle-up"></i></a>
        <div className="container">

          <Header {...this.props} />


          <section id="contentSection">
            <div className="row">
              <div className="col-lg-8 col-md-8 col-sm-8">
                <div className="left_content">
                  <div className="single_page">
                    <h1>{news.heading}</h1>
                    <div className="post_commentbox">
                      <span><i className="fa fa-calendar"></i>{(new Date).toString()}</span>
                    </div>
                    <div className="single_page_content">
                      <img className="img-center" src={news.image_url} alt="" />
                      <b>{news.heading}</b>
                      <br />
                      <br />
                      <p>{news.short_description}</p>
                      <p>{news.description}</p>
                    </div>
                  </div>
                </div>
              </div>



              <Advertisements />

            </div>
          </section>


        </div>

        <Footer />
      </div >
    )
  }
}

export default SingleNews;
